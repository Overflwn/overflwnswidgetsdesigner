#include "ledwidget.h"
#include "ledwidgetplugin.h"

#include <QtPlugin>

LedWidgetPlugin::LedWidgetPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void LedWidgetPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool LedWidgetPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *LedWidgetPlugin::createWidget(QWidget *parent)
{
    return new OverflwnsWidgets::LedWidget(parent);
}

QString LedWidgetPlugin::name() const
{
    return QLatin1String("OverflwnsWidgets::LedWidget");
}

QString LedWidgetPlugin::group() const
{
    return QLatin1String("Overflwn's Widgets");
}

QIcon LedWidgetPlugin::icon() const
{
    return QIcon();
}

QString LedWidgetPlugin::toolTip() const
{
    return QLatin1String("A simple LED");
}

QString LedWidgetPlugin::whatsThis() const
{
    return QLatin1String("An LED that can switch between a dark (off) and bright (on) color.");
}

bool LedWidgetPlugin::isContainer() const
{
    return false;
}

QString LedWidgetPlugin::domXml() const
{
    return QLatin1String("<widget class=\"OverflwnsWidgets::LedWidget\" name=\"ledWidget\">\n<property name=\"geometry\">\n  <rect>\n    <x>0</x>\n    <y>0</y>\n    <width>50</width>\n    <height>50</height>\n  </rect>\n</property>\n</widget>\n");
}

QString LedWidgetPlugin::includeFile() const
{
    return QLatin1String("ledwidget.h");
}

