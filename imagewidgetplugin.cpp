#include "imagewidget.h"
#include "imagewidgetplugin.h"

#include <QtPlugin>

ImageWidgetPlugin::ImageWidgetPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void ImageWidgetPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool ImageWidgetPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *ImageWidgetPlugin::createWidget(QWidget *parent)
{
    return new OverflwnsWidgets::ImageWidget(parent);
}

QString ImageWidgetPlugin::name() const
{
    return QLatin1String("OverflwnsWidgets::ImageWidget");
}

QString ImageWidgetPlugin::group() const
{
    return QLatin1String("Overflwn's Widgets");
}

QIcon ImageWidgetPlugin::icon() const
{
    return QIcon();
}

QString ImageWidgetPlugin::toolTip() const
{
    return QLatin1String("An image widget");
}

QString ImageWidgetPlugin::whatsThis() const
{
    return QLatin1String("A widget that can show an image from a QImage or raw byte data paired with a given pixel format.");
}

bool ImageWidgetPlugin::isContainer() const
{
    return false;
}

QString ImageWidgetPlugin::domXml() const
{
    return QLatin1String("<widget class=\"OverflwnsWidgets::ImageWidget\" name=\"imageWidget\">\n<property name=\"geometry\">\n  <rect>\n    <x>0</x>\n    <y>0</y>\n    <width>50</width>\n    <height>50</height>\n  </rect>\n</property>\n</widget>\n");
}

QString ImageWidgetPlugin::includeFile() const
{
    return QLatin1String("imagewidget.h");
}

