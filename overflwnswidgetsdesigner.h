#ifndef OVERFLWNSWIDGETSDESIGNER_H
#define OVERFLWNSWIDGETSDESIGNER_H

#include <QtDesigner/QtDesigner>
#include <qplugin.h>

class OverflwnsWidgetsDesigner : public QObject, public QDesignerCustomWidgetCollectionInterface
{
    Q_OBJECT
    Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QDesignerCustomWidgetCollectionInterface")
#endif // QT_VERSION >= 0x050000

public:
    explicit OverflwnsWidgetsDesigner(QObject *parent = 0);

    virtual QList<QDesignerCustomWidgetInterface*> customWidgets() const;

private:
    QList<QDesignerCustomWidgetInterface*> m_widgets;
};

#endif // OVERFLWNSWIDGETSDESIGNER_H
