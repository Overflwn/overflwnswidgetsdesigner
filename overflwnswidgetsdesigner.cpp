#include "ledwidgetplugin.h"
#include "imagewidgetplugin.h"
#include "overflwnswidgetsdesigner.h"

OverflwnsWidgetsDesigner::OverflwnsWidgetsDesigner(QObject *parent)
    : QObject(parent)
{
    m_widgets.append(new LedWidgetPlugin(this));
    m_widgets.append(new ImageWidgetPlugin(this));

}

QList<QDesignerCustomWidgetInterface*> OverflwnsWidgetsDesigner::customWidgets() const
{
    return m_widgets;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(overflwnswidgetsdesignerplugin, OverflwnsWidgetsDesigner)
#endif // QT_VERSION < 0x050000
